Source: djangocms-admin-style
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Michael Fladischer <fladi@debian.org>
Build-Depends: debhelper-compat (= 9),
               dh-python,
               python3-all,
               python3-pyscss,
               python3-babel,
               python3-setuptools
Standards-Version: 3.9.8
Homepage: http://www.django-cms.org/
Vcs-Browser: https://salsa.debian.org/python-team/packages/djangocms-admin-style
Vcs-Git: https://salsa.debian.org/python-team/packages/djangocms-admin-style.git

Package: python-djangocms-admin-style-common
Architecture: all
Depends: libjs-jquery-ui-touch-punch,
         libjs-pie,
         ${misc:Depends},
         ${python3:Depends}
Description: CSS styles for the django CMS admin interface (common assets)
 django CMS is a modern web publishing platform built with Django. It offers
 out-of-the-box support for the common features expected from a CMS, but can
 also be easily customised and extended by developers to create a site that is
 tailored to their precise needs.
 .
 django CMS is a well-tested CMS platform that powers sites both large and
 small. Here are a few of the key features:
  * robust internationalisation (i18n) support for creating multilingual sites
  * virtually unlimited undo history, allowing editors to revert to a previous
    version
  * front-end editing, providing rapid access to the content management
    interface
  * support for a variety of editors with advanced text editing features
  * a flexible plugins system that lets developers put powerful tools at the
    fingertips of editors, without overwhelming them with a difficult interface
 .
 This package contains the common assets of the admin styles required by
 django CMS.

Package: python3-djangocms-admin-style
Architecture: all
Depends: python-djangocms-admin-style-common (= ${binary:Version}),
         ${misc:Depends},
         ${python3:Depends}
Description: CSS styles for the django CMS admin interface (Python3 version)
 django CMS is a modern web publishing platform built with Django. It offers
 out-of-the-box support for the common features expected from a CMS, but can
 also be easily customised and extended by developers to create a site that is
 tailored to their precise needs.
 .
 django CMS is a well-tested CMS platform that powers sites both large and
 small. Here are a few of the key features:
  * robust internationalisation (i18n) support for creating multilingual sites
  * virtually unlimited undo history, allowing editors to revert to a previous
    version
  * front-end editing, providing rapid access to the content management
    interface
  * support for a variety of editors with advanced text editing features
  * a flexible plugins system that lets developers put powerful tools at the
    fingertips of editors, without overwhelming them with a difficult interface
 .
 This package contains the Python 3 version of the admin styles required by
 django CMS.
